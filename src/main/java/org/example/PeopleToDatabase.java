package org.example;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class PeopleToDatabase {

    public static void addPersonToTxt(Person person, String database, boolean append) {

        try {
            PrintWriter outputFile = new PrintWriter(new BufferedWriter(new FileWriter(database, append)));

            outputFile.println(person.getId() + " " + person.getName() + " " + person.getSurname() + " " + person.getAge());

            outputFile.close();

        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }

}
