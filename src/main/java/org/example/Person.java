package org.example;

public class Person {

    private String name;
    private String surname;
    private int age;

    private String id;

    public Person() {}

    public Person(String name, String surname, int age, String id) {
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.id = id;
    }

    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }


    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getAge() {
        return age;
    }


    public void setAge(int age) {
        this.age = age;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Name: " + name +
                "\nSurname: " + surname +
                "\nAge: " + age +
                "\nID: " + id;
    }

    public String toDatabaseString(){
        return id + " " + name + " " + surname + " " + age;
    }

}