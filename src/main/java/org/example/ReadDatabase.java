package org.example;

import java.io.*;

public class ReadDatabase {

    public static String read(String filepath) {

        try {
            BufferedReader outputFile = new BufferedReader(new FileReader(filepath));

            String st;
            String res = "";

            while ((st = outputFile.readLine()) != null) {
                System.out.println(st);
                res += st;
            }
            return res;

        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
        return null;
    }

}
