import org.example.PeopleToDatabase;
import org.example.Person;
import org.example.ReadDatabase;
import org.junit.Test;

import java.io.FileNotFoundException;

public class PersonTester {

    @Test
    public void createPerson() throws FileNotFoundException {

        Person person = new Person("Dima", "Parnak", 18, "#112");

        PeopleToDatabase.addPersonToTxt(person, "Test.txt", false);

        assert ReadDatabase.read("Test.txt").equals(person.toDatabaseString());
        System.out.println("Person added to database correctly");
    }
}
